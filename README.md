# Apache logs to CSV
This little script converts Apache 2 logs to CSV format.

Example input format:
```
138.246.253.15 - - [14/Jun/2020:06:25:25 +0200] "HEAD / HTTP/1.1" 400 -
45.56.104.43 - - [14/Jun/2020:06:26:20 +0200] "GET / HTTP/1.1" 302 220
```

Example output format:
```
ip,timestamp,http_request,http_status
138.246.253.15,14/Jun/2020:06:25:25 +0200,HEAD / HTTP/1.1,400
45.56.104.43,14/Jun/2020:06:26:20 +0200,GET / HTTP/1.1,302
```

More readable:

ip | timestamp | http_request | http_status
--- | --- | --- | ---
138.246.253.15 | 14/Jun/2020:06:25:25 +0200 | HEAD / HTTP/1.1 | 400
45.56.104.43 | 14/Jun/2020:06:26:20 +0200 | GET / HTTP/1.1 | 302

## Usage
```
$ python3 a22csv.py -h
usage: a22csv.py [-h] [-v] infile outfile

Parse Apache2 logs to CSV.

positional arguments:
  infile         Apache2 log file to parse.
  outfile        CSV file to write to.

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose  Enable debug logging to stderr. Expect lots of output.
```

## Installation
Clone the repo, create a venv in the repo dir, install wheel and the python snips package (required for an argparse action to open a CSV file) to run the script.
```
git clone https://codeberg.org/plenae/a22csv.git
cd a22csv
python3 -m venv venv
source venv/bin/activate
pip install wheel
pip install git+https://codeberg.org/plenae/python-snips.git
python3 a22csv.py
```

## License
Copyright (C) 2020 Pieter Lenaerts

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see (https://www.gnu.org/licenses/).
