# Apache to csv - parses Apache2 log files to csv
# Copyright (C) 2020 Pieter Lenaerts
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import re
import csv
import logging
import argparse
import sys
import snips.argparse_actions


fields = ['ip', 'timestamp', 'http_request', 'http_status']
p_ip = '([0-9]+\.){3}[0-9]+'
p_time = '\[(\d{2}\/\w{3}\/\d{4}:\d{2}:\d{2}:\d{2} \+\d{4})\]'
p_req = '"(.*)"'
p_status = '[\d\.]+ .+ .+ \[.+\] ".+" (.+) .+'


def get_args():
    parser = argparse.ArgumentParser(
            description="Parse Apache2 logs to CSV.")
    parser.add_argument('infile', type=argparse.FileType('r'),
            help='Apache2 log file to parse.')
    parser.add_argument('outfile', default=sys.stdout,
            type=snips.argparse_actions.FileNewlineType('w', newline=''),
            help='CSV file to write to.')
    parser.add_argument('-v', '--verbose', action='store_true',
            help='Enable debug logging to stderr. Expect lots of output.')
    return parser.parse_args()


def main():
    args = get_args()
    infile = args.infile
    outfile = csv.DictWriter(args.outfile, fields)
    outfile.writeheader()
    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.WARNING
    logging.basicConfig(level=log_level)
    i = 0
    ip = None
    timestamp = None
    http_request = None
    http_status = None
    for line in infile:
        i = i + 1
        logging.debug('Line: ' + str(i))
        m = re.match(p_ip, line)
        if m:
            ip = m[0]
            logging.debug(ip)
        m = re.search(p_time, line)
        if m:
            timestamp = m.groups()[0]
            logging.debug(timestamp)
        m = re.search(p_req, line)
        if m:
            http_request = m.groups()[0]
            logging.debug(http_request)
        m = re.search(p_status, line)
        if m:
            http_status = m.groups()[0]
            logging.debug(http_status)
        if ip and timestamp and http_request and http_status:
            outfile.writerow({'ip': ip, 'timestamp': timestamp, 'http_request': http_request, 'http_status': http_status})
        else:
            logging.warning('Error on line: ' + str(i))


if __name__ == '__main__':
    main()
